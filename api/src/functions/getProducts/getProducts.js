/* eslint-disable no-unused-vars */
import { shopifyApi } from '@shopify/shopify-api'

const querystring = require('querystring')

const request = require('request-promise')

const shopify = shopifyApi({
  apiKey: process.env.API_KEY,
  apiSecretKey: process.env.API_SECRET_KEY,
  scopes: ['read_products, write_products, read_themes, write_themes'],
  hostName: 'http://localhost:8910/',
})

export const handler = async (event, context) => {
  let sCode = 200
  let res = ''
  const { shop } = event.queryStringParameters

  const shopRequestURL =
    'https://' + shop + '.myshopify.com/admin/api/2022-10/graphql.json'
  const shopRequestHeaders = {
    'Content-Type': 'application/graphql',
    'X-Shopify-Access-Token': '',
  }

  await request
    .post(shopRequestURL, {
      headers: shopRequestHeaders,
      body: `{
      products(first: 5) {
        edges {
          node {
            id
            title
          }
        }
      }
    }`,
    })
    .then((shopResponse) => {
      console.log(shopResponse)
      sCode = 200
      res = shopResponse
    })
    .catch((error) => {
      sCode = error.statusCode
      res = error.error.error_description
      console.log(error)
      console.log(error.error.error_description)
    })

  return {
    statusCode: sCode,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
    body: { data: res },
  }
}
