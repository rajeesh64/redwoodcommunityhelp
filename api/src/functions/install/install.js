/* eslint-disable no-unused-vars */
import { shopifyApi } from '@shopify/shopify-api'

const shopify = shopifyApi({
  apiKey: process.env.API_KEY,
  apiSecretKey: process.env.API_SECRET_KEY,
  scopes: ['read_products, write_products, read_themes, write_themes'],
  hostName: 'http://localhost:8910/',
})

const redirectURL = 'http://localhost:8911/callback'

export const handler = async (event, context) => {
  let sCode = 200
  let res = ''
  const shopName = event.queryStringParameters.shop
  console.log(shopName)
  console.log(process.env.API_KEY)
  const shop = shopify.utils.sanitizeShop(shopName + '.myshopify.com')
  if (!shopName || !shop) {
    sCode = 500
    res = 'No shop provided'
  } else {
    sCode = 200
    res =
      'https://' +
      shop +
      '/admin/oauth/authorize?client_id=' +
      shopify.config.apiKey +
      '&scope=' +
      shopify.config.scopes +
      '&redirect_uri=' +
      redirectURL
  }
  return {
    statusCode: sCode,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
    body: { data: res },
  }
}
