/* eslint-disable no-unused-vars */
import { shopifyApi } from '@shopify/shopify-api'

const crypto = require('crypto')
const querystring = require('querystring')

const request = require('request-promise')

const shopify = shopifyApi({
  apiKey: process.env.API_KEY,
  apiSecretKey: process.env.API_SECRET_KEY,
  scopes: ['read_products, write_products, read_themes, write_themes'],
  hostName: 'http://localhost:8910/',
})

export const handler = async (event, context) => {
  let sCode = 200
  let res = ''
  const { shop, hmac, code } = event.queryStringParameters
  const providedHmac = Buffer.from(hmac, 'utf-8')
  const queryMap = { ...event.queryStringParameters }
  delete queryMap['hmac']
  const message = querystring.stringify(queryMap)
  const generatedHash = Buffer.from(
    crypto
      .createHmac('sha256', shopify.config.apiSecretKey)
      .update(message)
      .digest('hex'),
    'utf-8'
  )
  let hashEquals = false
  try {
    hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
  } catch (e) {
    hashEquals = false
  }
  if (!hashEquals) {
    sCode = 500
    res = 'HMAC validation failed'
    console.log('HMAC validation failed')
  }
  const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token'
  const accessTokenPayload = {
    client_id: shopify.config.apiKey,
    client_secret: shopify.config.apiSecretKey,
    code: code,
  }
  await request
    .post(accessTokenRequestUrl, { json: accessTokenPayload })
    .then(async (accessTokenResponse) => {
      const accessToken = accessTokenResponse.access_token
      console.log(accessToken)
      const shopRequestURL = 'https://' + shop + '/admin/api/2020-04/shop.json'
      const shopRequestHeaders = { 'X-Shopify-Access-Token': accessToken }

      await request
        .get(shopRequestURL, { headers: shopRequestHeaders })
        .then((shopResponse) => {
          console.log(shopResponse)
          sCode = 200
          res = 'https://' + shop + '/admin/apps' // Redirects after install
        })
        .catch((error) => {
          sCode = error.statusCode
          res = error.error.error_description
          console.log(error.statusCode)
          console.log(error.error.error_description)
        })
    })
    .catch((error) => {
      sCode = error.statusCode
      res = error.error.error_description
      console.log(error.statusCode)
      console.log(error.error.error_description)
    })
  return {
    statusCode: sCode,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
    body: { data: res },
  }
}
