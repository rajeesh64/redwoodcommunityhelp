import { db } from 'src/lib/db'

export async function getDataWebhook() {
  return await db.webhook.findMany().requestTransaction()
}

export async function getDataGroups() {
  return await db.groups.findMany().requestTransaction()
}
