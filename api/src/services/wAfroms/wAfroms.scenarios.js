export const standard = defineScenario({
  wAfrom: {
    one: {
      data: {
        site_id: 3654577,
        app_id: 6171016,
        phone_id: 300081,
        bussinus_id: 992116,
        access_token: 'String',
        expiry_in: '2022-12-15T09:49:35.325Z',
      },
    },
    two: {
      data: {
        site_id: 4683171,
        app_id: 1394235,
        phone_id: 8755794,
        bussinus_id: 8171552,
        access_token: 'String',
        expiry_in: '2022-12-15T09:49:35.325Z',
      },
    },
  },
})
