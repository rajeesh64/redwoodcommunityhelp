import {
  wAfroms,
  wAfrom,
  createWAfrom,
  updateWAfrom,
  deleteWAfrom,
} from './wAfroms'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('wAfroms', () => {
  scenario('returns all wAfroms', async (scenario) => {
    const result = await wAfroms()

    expect(result.length).toEqual(Object.keys(scenario.wAfrom).length)
  })

  scenario('returns a single wAfrom', async (scenario) => {
    const result = await wAfrom({ id: scenario.wAfrom.one.id })

    expect(result).toEqual(scenario.wAfrom.one)
  })

  scenario('creates a wAfrom', async () => {
    const result = await createWAfrom({
      input: {
        site_id: 7229083,
        app_id: 1466599,
        phone_id: 5960091,
        bussinus_id: 7932427,
        access_token: 'String',
        expiry_in: '2022-12-15T09:49:35.280Z',
      },
    })

    expect(result.site_id).toEqual(7229083)
    expect(result.app_id).toEqual(1466599)
    expect(result.phone_id).toEqual(5960091)
    expect(result.bussinus_id).toEqual(7932427)
    expect(result.access_token).toEqual('String')
    expect(result.expiry_in).toEqual(new Date('2022-12-15T09:49:35.280Z'))
  })

  scenario('updates a wAfrom', async (scenario) => {
    const original = await wAfrom({ id: scenario.wAfrom.one.id })
    const result = await updateWAfrom({
      id: original.id,
      input: { site_id: 660404 },
    })

    expect(result.site_id).toEqual(660404)
  })

  scenario('deletes a wAfrom', async (scenario) => {
    const original = await deleteWAfrom({
      id: scenario.wAfrom.one.id,
    })
    const result = await wAfrom({ id: original.id })

    expect(result).toEqual(null)
  })
})
