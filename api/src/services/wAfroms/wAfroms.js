import { db } from 'src/lib/db'

export const wAfroms = () => {
  return db.wAfrom.findMany()
}

export const wAfrom = ({ id }) => {
  return db.wAfrom.findUnique({
    where: { id },
  })
}

export const createWAfrom = ({ input }) => {
  return db.wAfrom.create({
    data: input,
  })
}

export const updateWAfrom = ({ id, input }) => {
  return db.wAfrom.update({
    data: input,
    where: { id },
  })
}

export const deleteWAfrom = ({ id }) => {
  return db.wAfrom.delete({
    where: { id },
  })
}
