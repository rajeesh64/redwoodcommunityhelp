import { sites, site, createSite, updateSite, deleteSite } from './sites'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('sites', () => {
  scenario('returns all sites', async (scenario) => {
    const result = await sites()

    expect(result.length).toEqual(Object.keys(scenario.site).length)
  })

  scenario('returns a single site', async (scenario) => {
    const result = await site({ id: scenario.site.one.id })

    expect(result).toEqual(scenario.site.one)
  })

  scenario('creates a site', async () => {
    const result = await createSite({
      input: { siteurl: 'String', access_token: 'String' },
    })

    expect(result.siteurl).toEqual('String')
    expect(result.access_token).toEqual('String')
  })

  scenario('updates a site', async (scenario) => {
    const original = await site({ id: scenario.site.one.id })
    const result = await updateSite({
      id: original.id,
      input: { siteurl: 'String2' },
    })

    expect(result.siteurl).toEqual('String2')
  })

  scenario('deletes a site', async (scenario) => {
    const original = await deleteSite({ id: scenario.site.one.id })
    const result = await site({ id: original.id })

    expect(result).toEqual(null)
  })
})
