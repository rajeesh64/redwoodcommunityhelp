export const standard = defineScenario({
  log: {
    one: {
      data: {
        site_id: 9457641,
        wa_to_id: 9093422,
        hook_id: 7235310,
        message: 'String',
      },
    },
    two: {
      data: {
        site_id: 8554687,
        wa_to_id: 4316565,
        hook_id: 5046196,
        message: 'String',
      },
    },
  },
})
