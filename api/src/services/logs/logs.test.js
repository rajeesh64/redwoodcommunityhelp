import { logs, log, createLog, updateLog, deleteLog } from './logs'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('logs', () => {
  scenario('returns all logs', async (scenario) => {
    const result = await logs()

    expect(result.length).toEqual(Object.keys(scenario.log).length)
  })

  scenario('returns a single log', async (scenario) => {
    const result = await log({ id: scenario.log.one.id })

    expect(result).toEqual(scenario.log.one)
  })

  scenario('creates a log', async () => {
    const result = await createLog({
      input: {
        site_id: 7235594,
        wa_to_id: 3322768,
        hook_id: 1250098,
        message: 'String',
      },
    })

    expect(result.site_id).toEqual(7235594)
    expect(result.wa_to_id).toEqual(3322768)
    expect(result.hook_id).toEqual(1250098)
    expect(result.message).toEqual('String')
  })

  scenario('updates a log', async (scenario) => {
    const original = await log({ id: scenario.log.one.id })
    const result = await updateLog({
      id: original.id,
      input: { site_id: 927539 },
    })

    expect(result.site_id).toEqual(927539)
  })

  scenario('deletes a log', async (scenario) => {
    const original = await deleteLog({ id: scenario.log.one.id })
    const result = await log({ id: original.id })

    expect(result).toEqual(null)
  })
})
