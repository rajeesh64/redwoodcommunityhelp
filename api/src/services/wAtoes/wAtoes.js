import { db } from 'src/lib/db'

export const wAtoes = () => {
  return db.wAto.findMany()
}

export const wAto = ({ id }) => {
  return db.wAto.findUnique({
    where: { id },
  })
}

export const createWAto = ({ input }) => {
  return db.wAto.create({
    data: input,
  })
}

export const updateWAto = ({ id, input }) => {
  return db.wAto.update({
    data: input,
    where: { id },
  })
}

export const deleteWAto = ({ id }) => {
  return db.wAto.delete({
    where: { id },
  })
}
