export const standard = defineScenario({
  wAto: {
    one: {
      data: {
        site_id: 9943034,
        number: 1351488,
        hooks: 'String',
        template: 'String',
      },
    },
    two: {
      data: {
        site_id: 6482900,
        number: 8062675,
        hooks: 'String',
        template: 'String',
      },
    },
  },
})
