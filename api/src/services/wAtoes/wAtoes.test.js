import { wAtoes, wAto, createWAto, updateWAto, deleteWAto } from './wAtoes'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('wAtoes', () => {
  scenario('returns all wAtoes', async (scenario) => {
    const result = await wAtoes()

    expect(result.length).toEqual(Object.keys(scenario.wAto).length)
  })

  scenario('returns a single wAto', async (scenario) => {
    const result = await wAto({ id: scenario.wAto.one.id })

    expect(result).toEqual(scenario.wAto.one)
  })

  scenario('creates a wAto', async () => {
    const result = await createWAto({
      input: {
        site_id: 5734013,
        number: 8123679,
        hooks: 'String',
        template: 'String',
      },
    })

    expect(result.site_id).toEqual(5734013)
    expect(result.number).toEqual(8123679)
    expect(result.hooks).toEqual('String')
    expect(result.template).toEqual('String')
  })

  scenario('updates a wAto', async (scenario) => {
    const original = await wAto({ id: scenario.wAto.one.id })
    const result = await updateWAto({
      id: original.id,
      input: { site_id: 3360821 },
    })

    expect(result.site_id).toEqual(3360821)
  })

  scenario('deletes a wAto', async (scenario) => {
    const original = await deleteWAto({ id: scenario.wAto.one.id })
    const result = await wAto({ id: original.id })

    expect(result).toEqual(null)
  })
})
