import {
  groupses,
  groups,
  createGroups,
  updateGroups,
  deleteGroups,
} from './groupses'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('groupses', () => {
  scenario('returns all groupses', async (scenario) => {
    const result = await groupses()

    expect(result.length).toEqual(Object.keys(scenario.groups).length)
  })

  scenario('returns a single groups', async (scenario) => {
    const result = await groups({ id: scenario.groups.one.id })

    expect(result).toEqual(scenario.groups.one)
  })

  scenario('creates a groups', async () => {
    const result = await createGroups({
      input: { name: 'String' },
    })

    expect(result.name).toEqual('String')
  })

  scenario('updates a groups', async (scenario) => {
    const original = await groups({ id: scenario.groups.one.id })
    const result = await updateGroups({
      id: original.id,
      input: { name: 'String2' },
    })

    expect(result.name).toEqual('String2')
  })

  scenario('deletes a groups', async (scenario) => {
    const original = await deleteGroups({
      id: scenario.groups.one.id,
    })
    const result = await groups({ id: original.id })

    expect(result).toEqual(null)
  })
})
