import { db } from 'src/lib/db'

export const groupses = () => {
  return db.groups.findMany()
}

export const groups = ({ id }) => {
  return db.groups.findUnique({
    where: { id },
  })
}

export const createGroups = ({ input }) => {
  return db.groups.create({
    data: input,
  })
}

export const updateGroups = ({ id, input }) => {
  return db.groups.update({
    data: input,
    where: { id },
  })
}

export const deleteGroups = ({ id }) => {
  return db.groups.delete({
    where: { id },
  })
}
