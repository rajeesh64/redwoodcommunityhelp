import { db } from 'src/lib/db'

export const webhooks = () => {
  return db.webhook.findMany()
}

export const webhook = ({ id }) => {
  return db.webhook.findUnique({
    where: { id },
  })
}

export const createWebhook = ({ input }) => {
  return db.webhook.create({
    data: input,
  })
}

export const updateWebhook = ({ id, input }) => {
  return db.webhook.update({
    data: input,
    where: { id },
  })
}

export const deleteWebhook = ({ id }) => {
  return db.webhook.delete({
    where: { id },
  })
}
