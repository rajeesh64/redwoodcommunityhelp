import {
  webhooks,
  webhook,
  createWebhook,
  updateWebhook,
  deleteWebhook,
} from './webhooks'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('webhooks', () => {
  scenario('returns all webhooks', async (scenario) => {
    const result = await webhooks()

    expect(result.length).toEqual(Object.keys(scenario.webhook).length)
  })

  scenario('returns a single webhook', async (scenario) => {
    const result = await webhook({ id: scenario.webhook.one.id })

    expect(result).toEqual(scenario.webhook.one)
  })

  scenario('creates a webhook', async () => {
    const result = await createWebhook({
      input: { group_id: 2506047, hook_name: 'String', hook_value: 'String' },
    })

    expect(result.group_id).toEqual(2506047)
    expect(result.hook_name).toEqual('String')
    expect(result.hook_value).toEqual('String')
  })

  scenario('updates a webhook', async (scenario) => {
    const original = await webhook({ id: scenario.webhook.one.id })
    const result = await updateWebhook({
      id: original.id,
      input: { group_id: 3557596 },
    })

    expect(result.group_id).toEqual(3557596)
  })

  scenario('deletes a webhook', async (scenario) => {
    const original = await deleteWebhook({
      id: scenario.webhook.one.id,
    })
    const result = await webhook({ id: original.id })

    expect(result).toEqual(null)
  })
})
