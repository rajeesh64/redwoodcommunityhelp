export const standard = defineScenario({
  webhook: {
    one: {
      data: { group_id: 8785325, hook_name: 'String', hook_value: 'String' },
    },
    two: {
      data: { group_id: 8405591, hook_name: 'String', hook_value: 'String' },
    },
  },
})
