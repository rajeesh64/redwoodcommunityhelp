export const schema = gql`
  type Log {
    id: Int!
    site_id: Int!
    wa_to_id: Int!
    hook_id: Int!
    message: String!
    sent: Int!
    created_date: DateTime!
    modified_date: DateTime!
  }

  type Query {
    logs: [Log!]! @requireAuth
    log(id: Int!): Log @requireAuth
  }

  input CreateLogInput {
    site_id: Int!
    wa_to_id: Int!
    hook_id: Int!
    message: String!
    sent: Int!
    created_date: DateTime!
    modified_date: DateTime!
  }

  input UpdateLogInput {
    site_id: Int
    wa_to_id: Int
    hook_id: Int
    message: String
    sent: Int
    created_date: DateTime
    modified_date: DateTime
  }

  type Mutation {
    createLog(input: CreateLogInput!): Log! @requireAuth
    updateLog(id: Int!, input: UpdateLogInput!): Log! @requireAuth
    deleteLog(id: Int!): Log! @requireAuth
  }
`
