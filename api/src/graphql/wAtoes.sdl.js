export const schema = gql`
  type WAto {
    id: Int!
    site_id: Int!
    number: Int!
    hooks: String!
    template: String!
    created_date: DateTime!
    modified_date: DateTime!
  }

  type Query {
    wAtoes: [WAto!]! @requireAuth
    wAto(id: Int!): WAto @requireAuth
  }

  input CreateWAtoInput {
    site_id: Int!
    number: Int!
    hooks: String!
    template: String!
    created_date: DateTime!
    modified_date: DateTime!
  }

  input UpdateWAtoInput {
    site_id: Int
    number: Int
    hooks: String
    template: String
    created_date: DateTime
    modified_date: DateTime
  }

  type Mutation {
    createWAto(input: CreateWAtoInput!): WAto! @requireAuth
    updateWAto(id: Int!, input: UpdateWAtoInput!): WAto! @requireAuth
    deleteWAto(id: Int!): WAto! @requireAuth
  }
`
