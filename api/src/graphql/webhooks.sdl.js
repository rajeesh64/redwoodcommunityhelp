export const schema = gql`
  type Webhook {
    id: Int!
    group_id: Int!
    hook_name: String!
    hook_value: String!
    platform: String
    created_date: DateTime!
  }

  type Query {
    webhooks: [Webhook!]! @requireAuth
    webhook(id: Int!): Webhook @requireAuth
  }

  input CreateWebhookInput {
    group_id: Int!
    hook_name: String!
    hook_value: String!
    platform: String
    created_date: DateTime!
  }

  input UpdateWebhookInput {
    group_id: Int
    hook_name: String
    hook_value: String
    platform: String
    created_date: DateTime
  }

  type Mutation {
    createWebhook(input: CreateWebhookInput!): Webhook! @requireAuth
    updateWebhook(id: Int!, input: UpdateWebhookInput!): Webhook! @requireAuth
    deleteWebhook(id: Int!): Webhook! @requireAuth
  }
`
