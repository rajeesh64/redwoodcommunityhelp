export const schema = gql`
  type Groups {
    id: Int!
    name: String!
    created_date: DateTime!
  }

  type Query {
    groupses: [Groups!]! @requireAuth
    groups(id: Int!): Groups @requireAuth
  }

  input CreateGroupsInput {
    name: String!
    created_date: DateTime!
  }

  input UpdateGroupsInput {
    name: String
    created_date: DateTime
  }

  type Mutation {
    createGroups(input: CreateGroupsInput!): Groups! @requireAuth
    updateGroups(id: Int!, input: UpdateGroupsInput!): Groups! @requireAuth
    deleteGroups(id: Int!): Groups! @requireAuth
  }
`
