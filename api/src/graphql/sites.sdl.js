export const schema = gql`
  type Site {
    id: Int!
    siteurl: String!
    platform: String
    access_token: String!
    active: Int!
    created_date: DateTime!
    modified_date: DateTime!
  }

  type Query {
    sites: [Site!]! @requireAuth
    site(id: Int!): Site @requireAuth
  }

  input CreateSiteInput {
    siteurl: String!
    platform: String
    access_token: String!
    active: Int!
    created_date: DateTime!
    modified_date: DateTime!
  }

  input UpdateSiteInput {
    siteurl: String
    platform: String
    access_token: String
    active: Int
    created_date: DateTime
    modified_date: DateTime
  }

  type Mutation {
    createSite(input: CreateSiteInput!): Site! @requireAuth
    updateSite(id: Int!, input: UpdateSiteInput!): Site! @requireAuth
    deleteSite(id: Int!): Site! @requireAuth
  }
`
