export const schema = gql`
  type WAfrom {
    id: Int!
    site_id: Int!
    app_id: Int!
    phone_id: Int!
    bussinus_id: Int!
    access_token: String!
    expiry_in: DateTime!
    created_date: DateTime!
    modified_date: DateTime!
  }

  type Query {
    wAfroms: [WAfrom!]! @requireAuth
    wAfrom(id: Int!): WAfrom @requireAuth
  }

  input CreateWAfromInput {
    site_id: Int!
    app_id: Int!
    phone_id: Int!
    bussinus_id: Int!
    access_token: String!
    expiry_in: DateTime!
    created_date: DateTime!
    modified_date: DateTime!
  }

  input UpdateWAfromInput {
    site_id: Int
    app_id: Int
    phone_id: Int
    bussinus_id: Int
    access_token: String
    expiry_in: DateTime
    created_date: DateTime
    modified_date: DateTime
  }

  type Mutation {
    createWAfrom(input: CreateWAfromInput!): WAfrom! @requireAuth
    updateWAfrom(id: Int!, input: UpdateWAfromInput!): WAfrom! @requireAuth
    deleteWAfrom(id: Int!): WAfrom! @requireAuth
  }
`
