/*
  Warnings:

  - Added the required column `group_id` to the `Webhook` table without a default value. This is not possible if the table is not empty.

*/
-- CreateTable
CREATE TABLE "Groups" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "created_date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Webhook" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "group_id" INTEGER NOT NULL,
    "hook_name" TEXT NOT NULL,
    "hook_value" TEXT NOT NULL,
    "platform" TEXT DEFAULT 'Shopify',
    "created_date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
INSERT INTO "new_Webhook" ("created_date", "hook_name", "hook_value", "id", "platform") SELECT "created_date", "hook_name", "hook_value", "id", "platform" FROM "Webhook";
DROP TABLE "Webhook";
ALTER TABLE "new_Webhook" RENAME TO "Webhook";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
