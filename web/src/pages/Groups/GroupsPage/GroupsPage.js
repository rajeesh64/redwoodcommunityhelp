import GroupsCell from 'src/components/Groups/GroupsCell'

const GroupsPage = ({ id }) => {
  return <GroupsCell id={id} />
}

export default GroupsPage
