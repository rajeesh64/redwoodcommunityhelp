import EditGroupsCell from 'src/components/Groups/EditGroupsCell'

const EditGroupsPage = ({ id }) => {
  return <EditGroupsCell id={id} />
}

export default EditGroupsPage
