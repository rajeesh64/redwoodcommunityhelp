import { render } from '@redwoodjs/testing/web'

import InstallPage from './InstallPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('InstallPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<InstallPage />)
    }).not.toThrow()
  })
})
