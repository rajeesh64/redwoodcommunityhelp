const InstallPage = () => {
  fetch('http://localhost:8911/install' + window.location.search)
    .then(function (response) {
      console.log(response.status)
      if (response.status == 200) return response.json()
      else return null
    })
    .then(function (body) {
      if (body != null) {
        console.log(body.data)
        window.location.replace(body.data)
      }
    })

  return (
    <>
      <center>{'Missing shop parameter!'}</center>
    </>
  )
}

export default InstallPage
