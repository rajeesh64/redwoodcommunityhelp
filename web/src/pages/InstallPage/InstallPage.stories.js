import InstallPage from './InstallPage'

export const generated = () => {
  return <InstallPage />
}

export default {
  title: 'Pages/InstallPage',
  component: InstallPage,
}
