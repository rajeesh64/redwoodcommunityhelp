const CallbackPage = () => {
  fetch('http://localhost:8911/callback' + window.location.search)
    .then(function (response) {
      console.log(response.status)
      if (response.status == 200) return response.json()
      else return null
    })
    .then(function (body) {
      if (body != null) {
        console.log(body.data)
        window.location.replace(body.data)
      }
    })

  return <></>
}

export default CallbackPage
