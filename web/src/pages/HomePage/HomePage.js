import { useState } from 'react'

import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'

import SettingsIcon from '@mui/icons-material/Settings'
import { FormControlLabel, FormGroup, Grid } from '@mui/material'
import Button from '@mui/material/Button'
import Card from '@mui/material/Card'
import CardActions from '@mui/material/CardActions'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Checkbox from '@mui/material/Checkbox'
import Paper from '@mui/material/Paper'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import { Stack } from '@mui/system'
import TextField from '@mui/material/TextField';
// import { Form, TextAreaField, Submit } from '@redwoodjs/forms'

// import type { FindWebhooks } from 'types/graphql'
// import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import {
  useNavigate,
  TitleBar,
  Loading,
  Provider,
} from '@shopify/app-bridge-react'
import {
  AppProvider,
  EmptyState,
  Layout,
  Page,
  SkeletonBodyText,
} from '@shopify/polaris'

import Group from 'src/components/GroupsesCell'
import Webhooks from 'src/components/WebhooksCell'

// import * as alluser from 'src/services/webhook/webhooks'

import banner from './settings_page_banner.jpg'
// const isloaded = false
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  width: '50%',
  textAlign: 'center',
  backgroundColor: 'white',
  color: theme.palette.text.secondary,
}))

const HomePage = () => {
  const [data, setData] = useState([])
  const [group, setGrp] = useState([])
  const [isloaded, setloaded] = useState(false)
  // const navigate = useNavigate()
  const isLoading = true
  const isRefetching = false
  const QRCodes = []
  // console.log(Webhooks)
  /* loadingMarkup uses the loading component from AppBridge and components from Polaris  */
  const loadingMarkup = isLoading ? (
    <Card sectioned>
      <Loading />
      <SkeletonBodyText />
    </Card>
  ) : null

  const emptyStateMarkup =
    !isLoading && !QRCodes?.length ? (
      <Card sectioned>
        <EmptyState
          heading="Create unique QR codes for your product"
          /* This button will take the user to a Create a QR code page */
          action={{
            content: 'Create QR code',
            onAction: () => navigate('/qrcodes/new'),
          }}
          image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
        >
          <p>
            Allow customers to scan codes and buy products using their phones.
          </p>
        </EmptyState>
      </Card>
    ) : null
  const config = {
    apiKey: '6b5326fe08ca348a32d6df4b4d35ddfc',
    host: 'http://localhost:8910/', //http://localhost:8910/
    shopOrigin: 'https://teststoreredwood.myshopify.com/', //'https://teststoreredwood.myshopify.com/',
    forceRedirect: true,
  }
  // console.log(config)
  // async function getJsonData() {
  //   const val = await fetch(
  //     'http://localhost:8911/getProducts?shop=teststoreredwood' // 'http://localhost:8911/getProducts?shop=teststoreredwood'
  //   )
  //     .then(function (response) {
  //       console.log(response.status)
  //       if (response.status == 200) return response.json()
  //       else return null
  //     })
  //     .then(async function (body) {
  //       if (body != null) {
  //         return JSON.parse(body.data)
  //       } else return []
  //     })
  //   setData(val.data.products.edges)
  //   data.map((dataKey) => {
  //     console.log(dataKey.node.id + ' ' + dataKey.node.title)
  //   })
  // }

  if (!isloaded) {
    const val = fetch('http://localhost:8911/displayWebhooks')
      .then(function (response) {
        // console.log(response.status)
        if (response.status == 200) return response.json()
        else return null
      })
      .then(function (body) {
        if (body != null) {
          // console.log(JSON.parse(body.group))
          // return JSON.parse(body.group)
          const grp = JSON.parse(body.group)
          const webhooks = JSON.parse(body.data)
          setData(webhooks)
          setGrp(grp)
          setloaded(true)
          return grp
        }
      })
    console.log(val)
  }
  function handleSubmit() {
    alert('An essay was submitted: ' + this.state.value);

  }
  // const val = getSettingsData()
  // getSettingsData()

  return (
    <>
      <Grid alignItems="center" justifyContent="center" container>
      <form method="post" onSubmit={handleSubmit}>
        <Card sx={{ maxWidth: 800, minWidth:600 }}>
          <CardMedia sx={{ height: 250 }} image={banner} title="green iguana" />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div" style={{marginBottom:"20px"}}>
              <SettingsIcon></SettingsIcon>Notification Settings
            </Typography><br />

            <TextField
            style={{width:"400px",marginBottom:"40px"}}
          required
          width="600"
          id="outlined-required"
          label="Whatsapp Number"
          defaultValue=""
          name='toWhatsappNumber'
        />
         <Typography variant="body2" color="text.secondary">
         Please Select the Hooks to send notifications
            </Typography>
            {data.length == 0 ? <p>Loading..</p> : group.map((gkey) => {
              return [<Typography gutterBottom variant="h6" marginTop="30px" component="div">{gkey.name}</Typography>,<hr />,
              data.map((wkey) => {
                if (gkey.id == wkey.group_id) {
                  // console.log(wkey.hook_name)
                  return [<div style={{marginLeft: "30px"}}><FormGroup>
                    <FormControlLabel
                      control={<Checkbox />} defaultChecked
                      label={wkey.hook_name}
                    />
                  </FormGroup></div>]
                }
              })]
            })}
          </CardContent>
          <CardActions style={{justifyContent: 'center'}}>
            <Button size="small" variant="contained" onClick={handleSubmit} >save</Button>
            {/* <Submit className="button">Save</Submit> */}
          </CardActions><br />
        </Card>
        </form>
      </Grid>

      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
    </>
  )
}

export default HomePage
