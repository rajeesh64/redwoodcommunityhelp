import EditWebhookCell from 'src/components/Webhook/EditWebhookCell'

const EditWebhookPage = ({ id }) => {
  return <EditWebhookCell id={id} />
}

export default EditWebhookPage
