import WebhookCell from 'src/components/Webhook/WebhookCell'

const WebhookPage = ({ id }) => {
  return <WebhookCell id={id} />
}

export default WebhookPage
