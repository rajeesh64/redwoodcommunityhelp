export const QUERY = gql`
  query FindWebhookQuery($id: Int!) {
    webhook: webhook(id: $id) {
      id
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }) => (
  <div style={{ color: 'red' }}>Error: {error?.message}</div>
)

export const Success = ({ webhook }) => {
  return <div>{JSON.stringify(webhook)}</div>
}
