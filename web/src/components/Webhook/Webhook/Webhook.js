import { Link, routes, navigate } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { timeTag } from 'src/lib/formatters'

const DELETE_WEBHOOK_MUTATION = gql`
  mutation DeleteWebhookMutation($id: Int!) {
    deleteWebhook(id: $id) {
      id
    }
  }
`

const Webhook = ({ webhook }) => {
  const [deleteWebhook] = useMutation(DELETE_WEBHOOK_MUTATION, {
    onCompleted: () => {
      toast.success('Webhook deleted')
      navigate(routes.webhooks())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete webhook ' + id + '?')) {
      deleteWebhook({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Webhook {webhook.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{webhook.id}</td>
            </tr>
            <tr>
              <th>Group id</th>
              <td>{webhook.group_id}</td>
            </tr>
            <tr>
              <th>Hook name</th>
              <td>{webhook.hook_name}</td>
            </tr>
            <tr>
              <th>Hook value</th>
              <td>{webhook.hook_value}</td>
            </tr>
            <tr>
              <th>Platform</th>
              <td>{webhook.platform}</td>
            </tr>
            <tr>
              <th>Created date</th>
              <td>{timeTag(webhook.created_date)}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editWebhook({ id: webhook.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(webhook.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Webhook
