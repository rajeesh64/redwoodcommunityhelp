import { Link, routes } from '@redwoodjs/router'

import Webhooks from 'src/components/Webhook/Webhooks'

export const QUERY = gql`
  query FindWebhooks {
    webhooks {
      id
      group_id
      hook_name
      hook_value
      platform
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No webhooks yet. '}
      <Link to={routes.newWebhook()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ webhooks }) => {
  return <Webhooks webhooks={webhooks} />
}
