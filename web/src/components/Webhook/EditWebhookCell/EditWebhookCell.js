import { navigate, routes } from '@redwoodjs/router'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import WebhookForm from 'src/components/Webhook/WebhookForm'

export const QUERY = gql`
  query EditWebhookById($id: Int!) {
    webhook: webhook(id: $id) {
      id
      group_id
      hook_name
      hook_value
      platform
      created_date
    }
  }
`
const UPDATE_WEBHOOK_MUTATION = gql`
  mutation UpdateWebhookMutation($id: Int!, $input: UpdateWebhookInput!) {
    updateWebhook(id: $id, input: $input) {
      id
      group_id
      hook_name
      hook_value
      platform
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ webhook }) => {
  const [updateWebhook, { loading, error }] = useMutation(
    UPDATE_WEBHOOK_MUTATION,
    {
      onCompleted: () => {
        toast.success('Webhook updated')
        navigate(routes.webhooks())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input, id) => {
    updateWebhook({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Webhook {webhook?.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <WebhookForm
          webhook={webhook}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
