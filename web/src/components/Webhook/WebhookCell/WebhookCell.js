import Webhook from 'src/components/Webhook/Webhook'

export const QUERY = gql`
  query FindWebhookById($id: Int!) {
    webhook: webhook(id: $id) {
      id
      group_id
      hook_name
      hook_value
      platform
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Webhook not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ webhook }) => {
  return <Webhook webhook={webhook} />
}
