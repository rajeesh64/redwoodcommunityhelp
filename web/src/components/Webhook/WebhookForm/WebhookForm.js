import {
  Form,
  FormError,
  FieldError,
  Label,
  NumberField,
  TextField,
  Submit,
} from '@redwoodjs/forms'

const WebhookForm = (props) => {
  const onSubmit = (data) => {
    props.onSave(data, props?.webhook?.id)
  }

  return (
    <div className="rw-form-wrapper">
      <Form onSubmit={onSubmit} error={props.error}>
        <FormError
          error={props.error}
          wrapperClassName="rw-form-error-wrapper"
          titleClassName="rw-form-error-title"
          listClassName="rw-form-error-list"
        />

        <Label
          name="group_id"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Group id
        </Label>

        <NumberField
          name="group_id"
          defaultValue={props.webhook?.group_id}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="group_id" className="rw-field-error" />

        <Label
          name="hook_name"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Hook name
        </Label>

        <TextField
          name="hook_name"
          defaultValue={props.webhook?.hook_name}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="hook_name" className="rw-field-error" />

        <Label
          name="hook_value"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Hook value
        </Label>

        <TextField
          name="hook_value"
          defaultValue={props.webhook?.hook_value}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
          validation={{ required: true }}
        />

        <FieldError name="hook_value" className="rw-field-error" />

        <Label
          name="platform"
          className="rw-label"
          errorClassName="rw-label rw-label-error"
        >
          Platform
        </Label>

        <TextField
          name="platform"
          defaultValue={props.webhook?.platform}
          className="rw-input"
          errorClassName="rw-input rw-input-error"
        />

        <FieldError name="platform" className="rw-field-error" />

        <div className="rw-button-group">
          <Submit disabled={props.loading} className="rw-button rw-button-blue">
            Save
          </Submit>
        </div>
      </Form>
    </div>
  )
}

export default WebhookForm
