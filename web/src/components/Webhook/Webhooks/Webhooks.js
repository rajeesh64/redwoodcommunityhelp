import { Link, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { QUERY } from 'src/components/Webhook/WebhooksCell'
import { timeTag, truncate } from 'src/lib/formatters'

const DELETE_WEBHOOK_MUTATION = gql`
  mutation DeleteWebhookMutation($id: Int!) {
    deleteWebhook(id: $id) {
      id
    }
  }
`

const WebhooksList = ({ webhooks }) => {
  const [deleteWebhook] = useMutation(DELETE_WEBHOOK_MUTATION, {
    onCompleted: () => {
      toast.success('Webhook deleted')
    },
    onError: (error) => {
      toast.error(error.message)
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete webhook ' + id + '?')) {
      deleteWebhook({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Group id</th>
            <th>Hook name</th>
            <th>Hook value</th>
            <th>Platform</th>
            <th>Created date</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {webhooks.map((webhook) => (
            <tr key={webhook.id}>
              <td>{truncate(webhook.id)}</td>
              <td>{truncate(webhook.group_id)}</td>
              <td>{truncate(webhook.hook_name)}</td>
              <td>{truncate(webhook.hook_value)}</td>
              <td>{truncate(webhook.platform)}</td>
              <td>{timeTag(webhook.created_date)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.webhook({ id: webhook.id })}
                    title={'Show webhook ' + webhook.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editWebhook({ id: webhook.id })}
                    title={'Edit webhook ' + webhook.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <button
                    type="button"
                    title={'Delete webhook ' + webhook.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(webhook.id)}
                  >
                    Delete
                  </button>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default WebhooksList
