export const QUERY = gql`
  query GroupsesQuery {
    groupses {
      id
      name
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }) => (
  <div style={{ color: 'red' }}>Error: {error?.message}</div>
)

export const Success = ({ groupses }) => {
  return (
    <>
      {groupses.map((item) => {
        // return <li key={item.id}>{JSON.stringify(item)}</li>
        return JSON.stringify(item)
      })}
    </>
  )
}
