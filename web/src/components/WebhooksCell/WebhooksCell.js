export const QUERY = gql`
  query WebhooksQuery {
    webhooks {
      id
      hook_name
      group_id
      hook_value
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }) => (
  <div style={{ color: 'red' }}>Error: {error?.message}</div>
)

export const Success = ({ webhooks }) => {
  return (
    <>
      {webhooks.map((item) => {
        // return <li key={item.id}>{JSON.stringify(item)}</li>
        return JSON.stringify(item)
      })}
    </>
  )
}
