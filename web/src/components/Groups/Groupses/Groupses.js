import { Link, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { QUERY } from 'src/components/Groups/GroupsesCell'
import { timeTag, truncate } from 'src/lib/formatters'

const DELETE_GROUPS_MUTATION = gql`
  mutation DeleteGroupsMutation($id: Int!) {
    deleteGroups(id: $id) {
      id
    }
  }
`

const GroupsesList = ({ groupses }) => {
  const [deleteGroups] = useMutation(DELETE_GROUPS_MUTATION, {
    onCompleted: () => {
      toast.success('Groups deleted')
    },
    onError: (error) => {
      toast.error(error.message)
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete groups ' + id + '?')) {
      deleteGroups({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Created date</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {groupses.map((groups) => (
            <tr key={groups.id}>
              <td>{truncate(groups.id)}</td>
              <td>{truncate(groups.name)}</td>
              <td>{timeTag(groups.created_date)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.groups({ id: groups.id })}
                    title={'Show groups ' + groups.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editGroups({ id: groups.id })}
                    title={'Edit groups ' + groups.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <button
                    type="button"
                    title={'Delete groups ' + groups.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(groups.id)}
                  >
                    Delete
                  </button>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default GroupsesList
