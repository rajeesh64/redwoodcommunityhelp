import { Link, routes, navigate } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import { timeTag } from 'src/lib/formatters'

const DELETE_GROUPS_MUTATION = gql`
  mutation DeleteGroupsMutation($id: Int!) {
    deleteGroups(id: $id) {
      id
    }
  }
`

const Groups = ({ groups }) => {
  const [deleteGroups] = useMutation(DELETE_GROUPS_MUTATION, {
    onCompleted: () => {
      toast.success('Groups deleted')
      navigate(routes.groupses())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete groups ' + id + '?')) {
      deleteGroups({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Groups {groups.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{groups.id}</td>
            </tr>
            <tr>
              <th>Name</th>
              <td>{groups.name}</td>
            </tr>
            <tr>
              <th>Created date</th>
              <td>{timeTag(groups.created_date)}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editGroups({ id: groups.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(groups.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Groups
