import { navigate, routes } from '@redwoodjs/router'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import GroupsForm from 'src/components/Groups/GroupsForm'

export const QUERY = gql`
  query EditGroupsById($id: Int!) {
    groups: groups(id: $id) {
      id
      name
      created_date
    }
  }
`
const UPDATE_GROUPS_MUTATION = gql`
  mutation UpdateGroupsMutation($id: Int!, $input: UpdateGroupsInput!) {
    updateGroups(id: $id, input: $input) {
      id
      name
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ groups }) => {
  const [updateGroups, { loading, error }] = useMutation(
    UPDATE_GROUPS_MUTATION,
    {
      onCompleted: () => {
        toast.success('Groups updated')
        navigate(routes.groupses())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input, id) => {
    updateGroups({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Groups {groups?.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <GroupsForm
          groups={groups}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
