import { navigate, routes } from '@redwoodjs/router'
import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'

import GroupsForm from 'src/components/Groups/GroupsForm'

const CREATE_GROUPS_MUTATION = gql`
  mutation CreateGroupsMutation($input: CreateGroupsInput!) {
    createGroups(input: $input) {
      id
    }
  }
`

const NewGroups = () => {
  const [createGroups, { loading, error }] = useMutation(
    CREATE_GROUPS_MUTATION,
    {
      onCompleted: () => {
        toast.success('Groups created')
        navigate(routes.groupses())
      },
      onError: (error) => {
        toast.error(error.message)
      },
    }
  )

  const onSave = (input) => {
    createGroups({ variables: { input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Groups</h2>
      </header>
      <div className="rw-segment-main">
        <GroupsForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewGroups
