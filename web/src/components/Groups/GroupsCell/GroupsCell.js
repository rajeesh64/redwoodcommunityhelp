import Groups from 'src/components/Groups/Groups'

export const QUERY = gql`
  query FindGroupsById($id: Int!) {
    groups: groups(id: $id) {
      id
      name
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Groups not found</div>

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ groups }) => {
  return <Groups groups={groups} />
}
