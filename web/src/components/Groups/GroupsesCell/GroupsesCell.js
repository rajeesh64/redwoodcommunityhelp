import { Link, routes } from '@redwoodjs/router'

import Groupses from 'src/components/Groups/Groupses'

export const QUERY = gql`
  query FindGroupses {
    groupses {
      id
      name
      created_date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No groupses yet. '}
      <Link to={routes.newGroups()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }) => (
  <div className="rw-cell-error">{error?.message}</div>
)

export const Success = ({ groupses }) => {
  return <Groupses groupses={groupses} />
}
