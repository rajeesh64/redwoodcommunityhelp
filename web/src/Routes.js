// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route } from '@redwoodjs/router'

import ScaffoldLayout from 'src/layouts/ScaffoldLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={ScaffoldLayout} title="Webhooks" titleTo="webhooks" buttonLabel="New Webhook" buttonTo="newWebhook">
        <Route path="/webhooks/new" page={WebhookNewWebhookPage} name="newWebhook" />
        <Route path="/webhooks/{id:Int}/edit" page={WebhookEditWebhookPage} name="editWebhook" />
        <Route path="/webhooks/{id:Int}" page={WebhookWebhookPage} name="webhook" />
        <Route path="/webhooks" page={WebhookWebhooksPage} name="webhooks" />
      </Set>
      <Set wrap={ScaffoldLayout} title="Groupses" titleTo="groupses" buttonLabel="New Groups" buttonTo="newGroups">
        <Route path="/groupses/new" page={GroupsNewGroupsPage} name="newGroups" />
        <Route path="/groupses/{id:Int}/edit" page={GroupsEditGroupsPage} name="editGroups" />
        <Route path="/groupses/{id:Int}" page={GroupsGroupsPage} name="groups" />
        <Route path="/groupses" page={GroupsGroupsesPage} name="groupses" />
      </Set>
      <Route path="/" page={HomePage} name="home" />
      <Route path="/callback" page={CallbackPage} name="callback" />
      <Route path="/install" page={InstallPage} name="install" />
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
